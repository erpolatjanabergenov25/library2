package uz.pdp.records.domain.model;

public enum RentStatus {
    RENTED,
    RETURNED;
}
