package uz.pdp.records.domain.model;

public enum UserRole {
    LIBRARIAN,
    USER
}
