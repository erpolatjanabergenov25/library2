package uz.pdp.records.domain.model;

import java.util.UUID;

public abstract class BaseModel {

    {
        this.id = UUID.randomUUID();
    }

    protected UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
