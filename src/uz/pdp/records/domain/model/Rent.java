package uz.pdp.records.domain.model;

import java.util.UUID;

public class Rent extends BaseModel {

    private UUID userId;
    private UUID bookId;
    private RentStatus status;

    public Rent(UUID userId, UUID bookId, RentStatus status) {
        this.userId = userId;
        this.bookId = bookId;
        this.status = status;
    }

    public RentStatus getStatus() {
        return status;
    }

    public void setStatus(RentStatus status) {
        this.status = status;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getBookId() {
        return bookId;
    }

    public void setBookId(UUID bookId) {
        this.bookId = bookId;
    }
}
