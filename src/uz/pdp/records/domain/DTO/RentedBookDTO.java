package uz.pdp.records.domain.DTO;

public record RentedBookDTO(String name, String author) {
}

