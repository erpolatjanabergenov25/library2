package uz.pdp.records.domain.DTO;

import uz.pdp.records.domain.model.Book;
import uz.pdp.records.domain.model.User;

import java.util.ArrayList;

public record BookRentersDTo(Book book, ArrayList<User> renters) {

}
