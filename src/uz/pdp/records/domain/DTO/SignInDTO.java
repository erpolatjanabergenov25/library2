package uz.pdp.records.domain.DTO;

public record SignInDTO(String username, String password) {
}
