package uz.pdp.records.repository.rent;

import uz.pdp.records.domain.model.Rent;
import uz.pdp.records.repository.BaseRepository;

import java.util.ArrayList;

public interface RentRepository extends BaseRepository<Rent> {

}
