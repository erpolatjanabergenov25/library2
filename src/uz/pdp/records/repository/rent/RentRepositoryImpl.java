package uz.pdp.records.repository.rent;

import uz.pdp.records.domain.model.Rent;

import java.util.ArrayList;
import java.util.UUID;

public class RentRepositoryImpl implements RentRepository {
    private final ArrayList<Rent> RENTS = new ArrayList<>();

    private static final RentRepositoryImpl instance = new RentRepositoryImpl();

    private RentRepositoryImpl() {
    }

    public static RentRepositoryImpl getInstance() {
        return instance;
    }

    @Override
    public int add(Rent rent) {
        RENTS.add(rent);
        return 1;
    }

    @Override
    public Rent findById(UUID id) {
        return null;
    }

    @Override
    public ArrayList<Rent> getAll() {
        return RENTS;
    }

    @Override
    public void remove(Rent rent) {

    }

    @Override
    public void removeById(UUID id) {

    }
}
