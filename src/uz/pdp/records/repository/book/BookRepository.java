package uz.pdp.records.repository.book;

import uz.pdp.records.domain.model.Book;
import uz.pdp.records.repository.BaseRepository;

import java.util.ArrayList;

public interface BookRepository extends BaseRepository<Book> {

}
