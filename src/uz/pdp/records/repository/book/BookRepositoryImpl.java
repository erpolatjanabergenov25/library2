package uz.pdp.records.repository.book;

import uz.pdp.records.domain.model.Book;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class BookRepositoryImpl implements BookRepository {
    private final ArrayList<Book> BOOKS = new ArrayList<>();
    private static final BookRepositoryImpl instance = new BookRepositoryImpl();

    private BookRepositoryImpl() {
    }

    public static BookRepositoryImpl getInstance() {
        return instance;
    }

    @Override
    public int add(Book book) {
        BOOKS.add(book);
        return 1;
    }

    @Override
    public Book findById(UUID id) {
        for (Book book : BOOKS) {
            if (Objects.equals(book.getId(),id)){
                return book;
            }
        }
        return null;
    }

    public ArrayList<Book> getAll() {
        return BOOKS;
    }

    @Override
    public void remove(Book book) {

    }

    @Override
    public void removeById(UUID id) {

    }


}
