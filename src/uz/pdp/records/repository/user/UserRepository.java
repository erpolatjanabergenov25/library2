package uz.pdp.records.repository.user;

import uz.pdp.records.domain.model.User;
import uz.pdp.records.repository.BaseRepository;

import java.util.ArrayList;

public interface UserRepository extends BaseRepository<User> {

}
