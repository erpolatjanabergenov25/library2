package uz.pdp.records.repository.user;

import uz.pdp.records.domain.model.User;

import java.util.ArrayList;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository{
    private final ArrayList<User> USERS = new ArrayList<>();

    private static final UserRepositoryImpl instance = new UserRepositoryImpl();

    private UserRepositoryImpl() {
    }

    public static UserRepositoryImpl getInstance() {
        return instance;
    }

    @Override
    public int add(User user) {
        return 0;
    }

    @Override
    public User findById(UUID id) {
        return null;
    }

    @Override
    public ArrayList<User> getAll() {
        return USERS;
    }

    @Override
    public void remove(User user) {

    }

    @Override
    public void removeById(UUID id) {

    }
}
