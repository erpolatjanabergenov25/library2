package uz.pdp.records.repository;

import java.util.ArrayList;
import java.util.UUID;

public interface BaseRepository<T> {
    int add(T t);
    T findById(UUID id);
    ArrayList<T> getAll();
    void  remove(T t);
    void removeById(UUID id);
}
