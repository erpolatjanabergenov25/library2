package uz.pdp.records.controller;

import uz.pdp.records.domain.model.Book;
import uz.pdp.records.domain.model.User;
import uz.pdp.records.repository.book.BookRepositoryImpl;
import uz.pdp.records.service.book.BookServiceImpl;
import uz.pdp.records.service.rent.RentServiceImpl;
import uz.pdp.records.service.user.UserServiceImpl;

import java.util.Scanner;

import static uz.pdp.records.controller.AuthController.authMenu;
import static uz.pdp.records.domain.model.UserRole.USER;

public class Main {

    public static Scanner scanNum = new Scanner(System.in);
    public static Scanner scanStr = new Scanner(System.in);

    public static UserServiceImpl userService = UserServiceImpl.getInstance();
    public static RentServiceImpl rentService = RentServiceImpl.getInstance();
    public static BookServiceImpl bookService = BookServiceImpl.getInstance();
    public static BookRepositoryImpl userRepository = BookRepositoryImpl.getInstance();
    public static BookRepositoryImpl rentRepository = BookRepositoryImpl.getInstance();
    public static BookRepositoryImpl bookRepository = BookRepositoryImpl.getInstance();

    static {


         userService.add(new User("we","we","we",USER));
         userService.add(new User("we","wer","wer",USER));

         bookService.add(new Book("book111","eeee","200"));
         bookService.add(new Book("book222","ffff","300"));
         bookService.add(new Book("book333","dddd","500"));
    }

    public static void main(String[] args) {

      authMenu();

    }
}
