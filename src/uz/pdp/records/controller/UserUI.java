package uz.pdp.records.controller;

import uz.pdp.records.domain.DTO.RentedBookDTO;
import uz.pdp.records.domain.model.Book;
import uz.pdp.records.domain.model.Rent;
import uz.pdp.records.domain.model.RentStatus;
import uz.pdp.records.domain.model.User;

import java.util.ArrayList;
import java.util.UUID;

import static uz.pdp.records.controller.Main.*;
import static uz.pdp.records.domain.model.RentStatus.RENTED;

public class UserUI {

    static void userMenu(User user) {
        int action = 0;
        while (true) {
            System.out.println("1. All books\t2. Rent book\t3. Return Book\t4. See my books\t5. All history\t0. Back");
            action = scanNum.nextInt();
            switch (action){
                case 1 -> {
                    allBooks();
                }
                case 2 -> {
                    rentBook(user);
                }
                case 3 -> {
                    returnBooks(user);
                }
                case 4 -> {
                    seeMyBook(user);
                }
                case 5 -> {
                    allHistory(user);
                }
                case 0 -> {
                    return;
                }
                default -> {
                    System.out.println("Wrong input");
                    return;
                }
            }
        }
    }

    private static void allHistory(User user) {
    }

    private static void seeMyBook(User user) {

    }

    private static void returnBooks(User user) {
        ArrayList<RentedBookDTO> userRents = rentService.getUserRents(user.getId());
        if (userRents.isEmpty()){
            System.out.println("No rented");
        }
        int i = 1;
        for (RentedBookDTO book : userRents) {
            System.out.println(i++ + ". " + book.name() + " || " + book.author());
        }
        System.out.println();
        System.out.print("Choose book: ");
        int index = scanNum.nextInt();
        String name = userRents.get(index - 1).name();

    }

    private static void rentBook(User user) {
        ArrayList<Book> books = allBooks();

            System.out.println("0. Back");
            System.out.print("Choose book: ");
            int index = scanNum.nextInt();
            if (index == 0){
                return;
            }
            if (index < 0 || index > books.size()) {
                System.out.println("Wrong input");
                 return;
            }
            UUID bookId = books.get(index - 1).getId();

            int add = rentService.add(new Rent(user.getId(), bookId, RENTED));
            if (add == 1) System.out.println("✅✅✅");

    }

    private static ArrayList<Book> allBooks() {
        ArrayList<Book> books = bookRepository.getAll();
        if (books.isEmpty()){
            System.out.println("No books");
        }
        int i = 1;
        for (Book book : books) {
            System.out.println(i++ + ". " + book.getName() + " || " + book.getAuthor());
        }
        System.out.println();
      return books;
    }
}
