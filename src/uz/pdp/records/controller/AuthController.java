package uz.pdp.records.controller;

import uz.pdp.records.domain.DTO.SignInDTO;
import uz.pdp.records.domain.model.User;
import uz.pdp.records.domain.model.UserRole;

import static uz.pdp.records.controller.Main.*;
import static uz.pdp.records.controller.UserUI.userMenu;

public class AuthController {
    public static void authMenu() {

        int action = 0;
        while (true) {
            System.out.println("1. Sign in\t2. Sign up\t0. Exit");
            action = scanNum.nextInt();
            switch (action){
                case 1 -> {
                    signIn();
                }
                case 2 -> {
                    signUp();
                }
                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
                default -> {
                    System.out.println("Wrong input");
                }
            }
        }
    }


    public static  void signUp(){
        System.out.print("Enter name: ");
        String name = scanStr.nextLine();

        System.out.print("Enter username: ");
        String username = scanStr.nextLine();

        System.out.print("Enter password: ");
        String password = scanStr.nextLine();

        User user = new User(name,username,password, UserRole.USER);
        if (userService.add(user) == -1){
            System.out.println("Bunaqa user oldindan bor");
        }else{
            System.out.println("✅✅✅✅");
        }
    }

    public static void signIn(){
        System.out.print("Enter username: ");
        String username = scanStr.nextLine();

        System.out.print("Enter password: ");
        String password = scanStr.nextLine();

        SignInDTO signInDTO = new SignInDTO(username,password);

        User user = userService.signIn(signInDTO);

        if (user == null){
            System.out.println("No such user found");
        }else{
            switch (user.getRole()){
                case USER -> {
                    userMenu(user);
                }
                case LIBRARIAN -> {

                }
            }
        }


    }


}
