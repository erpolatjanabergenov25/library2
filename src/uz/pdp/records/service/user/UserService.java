package uz.pdp.records.service.user;

import uz.pdp.records.domain.DTO.SignInDTO;
import uz.pdp.records.domain.model.User;
import uz.pdp.records.service.BaseService;

public interface UserService extends BaseService<User> {
    User signIn(SignInDTO signInDTO);
}
