package uz.pdp.records.service.user;

import uz.pdp.records.domain.DTO.SignInDTO;
import uz.pdp.records.domain.model.User;
import uz.pdp.records.repository.user.UserRepository;
import uz.pdp.records.repository.user.UserRepositoryImpl;

import java.util.Objects;

public class UserServiceImpl implements UserService{

    private static final UserServiceImpl instance = new UserServiceImpl();
    private UserServiceImpl() {
    }
    public static UserServiceImpl getInstance() {
        return instance;
    }

    private final UserRepositoryImpl userRepository = UserRepositoryImpl.getInstance();

    @Override
    public int add(User user) {
         if(checkUser(user.getUsername())){
          return -1;
        }
        userRepository.getAll().add(user);
        return 1;
    }

    private boolean checkUser(String username) {
        for (User user : userRepository.getAll()) {
            if (Objects.equals(user.getUsername(),username)){
                return true;
            }
        }
          return false;
    }

    @Override
    public User signIn(SignInDTO signInDTO) {
        for (User user : userRepository.getAll()) {
            if (Objects.equals(user.getUsername(),signInDTO.username())
                    && Objects.equals(user.getPassword(),signInDTO.password())){
                return user;
            }
        }
        return null;
    }
}
