package uz.pdp.records.service.book;

import uz.pdp.records.domain.model.Book;
import uz.pdp.records.service.BaseService;

public interface BookService extends BaseService<Book> {
}
