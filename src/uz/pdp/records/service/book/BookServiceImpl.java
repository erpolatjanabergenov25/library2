package uz.pdp.records.service.book;

import uz.pdp.records.domain.model.Book;
import uz.pdp.records.repository.book.BookRepositoryImpl;
import uz.pdp.records.service.BaseService;

public class BookServiceImpl implements BookService {

    private static final BookServiceImpl instance = new BookServiceImpl();

    private BookServiceImpl() {
    }

    public static BookServiceImpl getInstance() {
        return instance;
    }

    private final BookRepositoryImpl bookRepository = BookRepositoryImpl.getInstance();


    @Override
    public int add(Book book) {
        bookRepository.getAll().add(book);
        return 1;
    }
}
