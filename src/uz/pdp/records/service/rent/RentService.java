package uz.pdp.records.service.rent;

import uz.pdp.records.domain.DTO.RentedBookDTO;
import uz.pdp.records.domain.model.Rent;
import uz.pdp.records.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface RentService extends BaseService<Rent> {

    ArrayList<RentedBookDTO> getUserRents(UUID userId);

}
