package uz.pdp.records.service.rent;

import uz.pdp.records.domain.DTO.RentedBookDTO;
import uz.pdp.records.domain.model.Book;
import uz.pdp.records.domain.model.Rent;
import uz.pdp.records.repository.book.BookRepositoryImpl;
import uz.pdp.records.repository.rent.RentRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;


public class RentServiceImpl implements RentService{

    private static final RentServiceImpl instance = new RentServiceImpl();

    private RentServiceImpl() {
    }

    public static RentServiceImpl getInstance() {
        return instance;
    }

    private final RentRepositoryImpl rentRepository = RentRepositoryImpl.getInstance();
    private final BookRepositoryImpl bookRepository = BookRepositoryImpl.getInstance();

    @Override
    public int add(Rent rent) {
        rentRepository.add(rent);
        return 1;
    }

    @Override
    public ArrayList<RentedBookDTO> getUserRents(UUID userId) {
        ArrayList<RentedBookDTO> rents = new ArrayList<>();
        for (Rent rent : rentRepository.getAll()) {
            if (Objects.equals(rent.getUserId(), userId)) {
                Book book = bookRepository.findById(rent.getBookId());
                rents.add(new RentedBookDTO(book.getName(), book.getAuthor()));
            }
        }
        return rents;
    }
}
